env.DPE_DEPLOY_KEY
def buildDisplayName
def now = new Date()
def dmzConfigs = []
def dmzSecrets = []
def backendConfigs = []
def backendSecrets = []
def combinedConfigs = []
def combinedSecrets = []
def paramsPrepared = false

def splitParams(params2Split, type) {
   def paramValues = []
   params2Split.split(',').each {
    if (it.contains(type)) {
        paramValues.add(it.split('/')[0])
    }
  }
  paramValues
}

pipeline {
    agent {
        docker {
            image 'image-folder/my-docker-image'
            args '-u root -v $WORKSPACE/node_modules'
        }
    }

    environment {
       dev = 'true'
    }

    parameters {
      extendedChoice(description: '', multiSelectDelimiter: ',', name: 'Release', quoteValue: false, saveJSONParameterToFile: false, type: 'PT_TEXTBOX', value: 'enter the release number', visibleItemCount: 2)
      extendedChoice(description: '', multiSelectDelimiter: ',', name: 'DMZ', quoteValue: false, saveJSONParameterToFile: false, type: 'PT_MULTI_SELECT', value: 'agent/config,agent/secret,web/config,web/secret,nginx/config', visibleItemCount: 6)
      extendedChoice(description: '', multiSelectDelimiter: ',', name: 'BACKEND', quoteValue: false, saveJSONParameterToFile: false, type: 'PT_MULTI_SELECT', value: 'datacore/config,datacore/secret,sso/config,sso/secret,payments/config,payments/secret,providers/config,providers/secret,hnlp/config,hnlp/secret,health ontology/config,health ontology/secret', visibleItemCount: 15)
    }

    stages {
          stage('Prepare Environment') {
              steps {
                script{
                  currentBuild.displayName="$params.Release:${BUILD_NUMBER}"
                }
              }    
          }

          stage('Create Config and Secret Map files') {
              steps{
                  script {
                    currentBuild.displayName="$params.Release:${BUILD_NUMBER}"
                    println "Release: $params.Release"
                    dmzConfigs = splitParams(params.DMZ, "config")
                    dmzSecrets = splitParams(params.DMZ, "secret")
                    backendConfigs = splitParams(params.BACKEND, "config")
                    backendSecrets = splitParams(params.BACKEND, "secret")
                    paramsPrepared = true
                  }
                }
          }
          stage('Deploy Dev') {
            steps {
                script {
                  combinedSecrets.addAll(backendSecrets)
                  combinedSecrets.addAll(dmzSecrets)
                  combinedConfigs.addAll(backendConfigs)
                  combinedConfigs.addAll(dmzConfigs)
                  println("Configs backend/dmz")
                  println(combinedConfigs)
                  println("Secrets backend/dmz")
                  println(combinedSecrets)
               }
            }
          }
          stage('Deploy Test'){
            steps {
                 timeout(time:5, unit:'MINUTES') {
                     input message:'Approve deployment?', submitter: 'it-ops'
                 }
                script {
                  if ( !paramsPrepared ) {
                       currentBuild.displayName="$params.Release:${BUILD_NUMBER}"
                       dmzConfigs = splitParams(params.DMZ, "config")
                       dmzSecrets = splitParams(params.DMZ, "secret")
                       backendConfigs = splitParams(params.BACKEND, "config")
                       backendSecrets = splitParams(params.BACKEND, "secret")
                       combinedSecrets.addAll(backendSecrets)
                       combinedSecrets.addAll(dmzSecrets)
                       combinedConfigs.addAll(backendConfigs)
                       combinedConfigs.addAll(dmzConfigs)
                       paramsPrepared=true
                  }
                  println("Configs backend/dmz")
                  println(combinedConfigs)
                  println("Secrets backend/dmz")
                  println(combinedSecrets)
               }
            }
          }
          stage('Deploy Prod'){
                steps {
                 timeout(time:5, unit:'MINUTES') {
                     input message:'Approve deployment?', submitter: 'it-ops'
                 }
                script {
                  if ( !paramsPrepared ) {
                      currentBuild.displayName="$params.Release:${BUILD_NUMBER}"
                      backendConfigs = splitParams(params.BACKEND, "config")
                      backendSecrets = splitParams(params.BACKEND, "secret")
                      paramsPrepared=true
                  }
                  println("Configs backend")
                  println(backendConfigs)
                  println("Secrets backend")
                  println(backendSecrets)
               }
            }
          }
          stage('Deploy DMZ'){
            steps {
                timeout(time:5, unit:'MINUTES') {
                    input message:'Approve deployment?', submitter: 'it-ops'
                }
                script {
                  if ( !paramsPrepared ) {
                      currentBuild.displayName="$params.Release:${BUILD_NUMBER}"
                      dmzConfigs = splitParams(params.DMZ, "config")
                      dmzSecrets = splitParams(params.DMZ, "secret")
                      paramsPrepared=true
                  }
                  println("Configs dmz")
                  println(dmzConfigs)
                  println("Secrets dmz")
                  println(dmzSecrets)
               }
            }
          }
    }
}